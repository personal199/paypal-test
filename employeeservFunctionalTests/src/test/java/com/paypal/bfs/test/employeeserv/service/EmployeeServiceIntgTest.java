package com.paypal.bfs.test.employeeserv.service;

import com.paypal.bfs.test.employeeserv.api.model.Address;
import com.paypal.bfs.test.employeeserv.api.model.Employee;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
/**
 * Here we are just testing end to end flow. Specifically validation factory construction. Unit tests already covered all validator related cases.
 */
public class EmployeeServiceIntgTest {

    @Autowired
    private EmployeeService employeeService;

    @Test
    public void employeeShouldGetCreatedIfThereIsNoValidationErrors() throws ParseException {
        Employee employeeToSave = getValidEmployeeCreateRequest();
        Integer employee = employeeService.createEmployee(employeeToSave, "x-1234-intg");
        Assert.assertNotNull(employee);
        Employee employeeById = employeeService.getEmployeeById(employee.toString());
        Assert.assertNotNull(employeeById);
    }

    private Employee getValidEmployeeCreateRequest() throws ParseException {
        String dateOfBirthString = "1900-01-01";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date dataOfBirth = formatter.parse(dateOfBirthString);
        Address address = new Address();
        address.setLine1("line1");
        address.setLine2("line2");
        address.setZipCode("007007");
        address.setState("KA");
        address.setCountry("IN");
        address.setCity("Bengaluru");
        Employee employee = new Employee();
        employee.setFirstName("ben");
        employee.setLastName("10");
        employee.setDateOfBirth(dataOfBirth);
        employee.setAddress(address);
        return employee;
    }
}
