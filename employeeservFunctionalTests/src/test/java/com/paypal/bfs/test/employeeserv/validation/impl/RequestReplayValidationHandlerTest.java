package com.paypal.bfs.test.employeeserv.validation.impl;

import com.paypal.bfs.test.employeeserv.api.model.Address;
import com.paypal.bfs.test.employeeserv.api.model.Employee;
import com.paypal.bfs.test.employeeserv.dao.RequestTrackerRepository;
import com.paypal.bfs.test.employeeserv.entity.Entity;
import com.paypal.bfs.test.employeeserv.entity.Operation;
import com.paypal.bfs.test.employeeserv.entity.RequestTrackerEO;
import com.paypal.bfs.test.employeeserv.validation.base.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(BlockJUnit4ClassRunner.class)
@Slf4j
public class RequestReplayValidationHandlerTest {

    private RequestTrackerRepository requestTrackerRepository;

    private RequestReplayValidationHandler requestReplayValidationHandler;

    @Before
    public void before() {
        requestTrackerRepository = mock(RequestTrackerRepository.class);
        requestReplayValidationHandler = new RequestReplayValidationHandler();
        requestReplayValidationHandler.setRequestTrackerRepository(requestTrackerRepository);
    }

    @Test
    public void validationShouldFailIfTheRequestIsAReplayOfThePreviousRequest() {
        Employee employee = getValidEmployeeCreateRequest();
        ValidationContext validationContext = ValidationContext.builder()
                .requestId("asdettnvhshwq")
                .operation(Operation.CREATE)
                .entityType(Entity.EMPLOYEE)
                .entity(employee)
                .build();
        when(requestTrackerRepository.getBy(anyString(), any(Entity.class), any(Operation.class)))
                .thenReturn(new RequestTrackerEO());
        ValidationResult validationResult = requestReplayValidationHandler.handle(validationContext);
        Assert.assertEquals(ValidationStatus.FAILED, validationResult.getValidationStatus());
        Assert.assertEquals(Validator.REQUEST_REPLAY_VALIDATOR, validationResult.getValidator());
        Assert.assertEquals(ValidationErrorConstants.REQUEST_REPLAYED, validationResult.getFailureReason());
    }

    @Test
    public void validationShouldSucceedIfTheRequestIsNew() {
        Employee employee = getValidEmployeeCreateRequest();
        ValidationContext validationContext = ValidationContext.builder()
                .requestId("asdettnvhshwq")
                .operation(Operation.CREATE)
                .entityType(Entity.EMPLOYEE)
                .entity(employee)
                .build();
        when(requestTrackerRepository.getBy(anyString(), any(Entity.class), any(Operation.class)))
                .thenReturn(null);
        ValidationResult validationResult = requestReplayValidationHandler.handle(validationContext);
        Assert.assertEquals(ValidationStatus.SUCCESS, validationResult.getValidationStatus());
    }

    private Employee getValidEmployeeCreateRequest() {
        String dateOfBirthString = "1900-01-01";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date dataOfBirth = null;
        try {
            dataOfBirth = formatter.parse(dateOfBirthString);
        } catch (Exception e) {
            log.error("Error converting date", e);
        }
        Address address = new Address();
        address.setLine1("line1");
        address.setLine2("line2");
        address.setZipCode("007007");
        address.setState("KA");
        address.setCountry("IN");
        address.setCity("Bengaluru");
        Employee employee = new Employee();
        employee.setFirstName("ben");
        employee.setLastName("10");
        employee.setDateOfBirth(dataOfBirth);
        employee.setAddress(address);
        return employee;
    }
}
