package com.paypal.bfs.test.employeeserv.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.paypal.bfs.test.employeeserv.api.model.Address;
import com.paypal.bfs.test.employeeserv.api.model.Employee;
import com.paypal.bfs.test.employeeserv.error.EntityNotFoundException;
import com.paypal.bfs.test.employeeserv.error.ErrorMessageConstants;
import com.paypal.bfs.test.employeeserv.error.ValidationFailedException;
import com.paypal.bfs.test.employeeserv.service.EmployeeService;
import com.paypal.bfs.test.employeeserv.validation.base.ValidationResult;
import com.paypal.bfs.test.employeeserv.validation.base.Validator;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Slf4j
public class EmployeeResourceImplTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeService employeeService;

    @Test
    public void shouldGetBadRequestIfThereIsThereIsNoRequestIdHeader() throws Exception {
        Employee employee = getValidEmployeeCreateRequest();
        employee.setFirstName(null);
        mockMvc.perform(post("/v1/bfs/employees/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(employee)))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("errorCode", is(400)))
                .andReturn();
    }

    @Test
    public void shouldGetBadRequestIfThereIsValidationErrors() throws Exception {
        when(employeeService.createEmployee(any(Employee.class), anyString()))
                .thenThrow(new ValidationFailedException(ValidationResult.failed().at(Validator.EMPLOYEE_PAYLOAD_VALIDATOR)));
        Employee employee = getValidEmployeeCreateRequest();
        employee.setFirstName(null);
        mockMvc.perform(post("/v1/bfs/employees/")
                .contentType(MediaType.APPLICATION_JSON)
                .header("x-request-id", "x-1234")
                .content(new ObjectMapper().writeValueAsString(employee)))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("errorCode", is(400)))
                .andReturn();
    }

    @Test
    public void shouldGetBadRequestIfTheRequestIsMalformed() throws Exception {
        mockMvc.perform(post("/v1/bfs/employees/")
                .contentType(MediaType.APPLICATION_JSON)
                .header("x-request-id", "x-1234")
                .content("random string"))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("errorCode", is(400)))
                .andReturn();
    }

    @Test
    public void shouldGetNotFoundIfTheEmployeeIdNotValid() throws Exception {
        when(employeeService.getEmployeeById(anyString())).thenThrow(new ValidationFailedException(ValidationResult.failed().at(Validator.EMPLOYEE_EXISTENCE_VALIDATOR)));
        mockMvc.perform(get("/v1/bfs/employees/123456")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("errorCode", is(404)))
                .andReturn();
    }

    @Test
    public void shouldGetCreatedIfThereIsNoValidationErrors() throws Exception {
        when(employeeService.createEmployee(any(Employee.class), anyString()))
                .thenReturn(100001);
        Employee employee = getValidEmployeeCreateRequest();
        mockMvc.perform(post("/v1/bfs/employees/")
                .contentType(MediaType.APPLICATION_JSON)
                .header("x-request-id", "x-1234")
                .content(new ObjectMapper().writeValueAsString(employee)))
                .andExpect(status().isCreated())
                .andReturn();
    }

    @Test
    public void shouldGetEmployeeEntityIfTheEmployeeIdIsValid() throws Exception {
        Employee employee = getValidEmployeeCreateRequest();
        when(employeeService.getEmployeeById(anyString())).thenReturn(employee);
        mockMvc.perform(get("/v1/bfs/employees/123456")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void shouldGetBadRequestIfTheEmployeeEntityIsNotFoundAfterValidation() throws Exception {
        when(employeeService.getEmployeeById(anyString())).thenThrow(new EntityNotFoundException(String.format(ErrorMessageConstants.ENTITY_NOT_FOUND, "123456")));
        mockMvc.perform(get("/v1/bfs/employees/123456")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("errorCode", is(404)))
                .andReturn();
    }



    private Employee getValidEmployeeCreateRequest() {
        String dateOfBirthString = "1900-01-01";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date dataOfBirth = null;
        try {
            dataOfBirth = formatter.parse(dateOfBirthString);
        } catch (Exception e) {
            log.error("Error converting date", e);
        }
        Address address = new Address();
        address.setLine1("line1");
        address.setLine2("line2");
        address.setZipCode("007007");
        address.setState("KA");
        address.setCountry("IN");
        address.setCity("Bengaluru");
        Employee employee = new Employee();
        employee.setFirstName("ben");
        employee.setLastName("10");
        employee.setDateOfBirth(dataOfBirth);
        employee.setAddress(address);
        return employee;
    }
}
