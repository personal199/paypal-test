package com.paypal.bfs.test.employeeserv.validation.impl;

import com.paypal.bfs.test.employeeserv.dao.EmployeeRepository;
import com.paypal.bfs.test.employeeserv.entity.EmployeeEO;
import com.paypal.bfs.test.employeeserv.validation.base.*;
import net.bytebuddy.utility.RandomString;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.mockito.ArgumentMatchers;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(BlockJUnit4ClassRunner.class)
public class EmployeeExistenceValidationHandlerTest {

    private EmployeeRepository employeeRepository;

    private EmployeeExistenceValidationHandler employeeExistenceValidationHandler;

    private static final String EMPLOYEE_ID = "employeeId";

    @Before
    public void setup() {
        employeeRepository = mock(EmployeeRepository.class);
        employeeExistenceValidationHandler = new EmployeeExistenceValidationHandler();
        employeeExistenceValidationHandler.setEmployeeRepository(employeeRepository);
    }

    @Test
    public void validationShouldFailIfTheEmployeeIdIsNotAValidInteger() {
        Map<String, Object> additionalInformation = new HashMap<>();
        String malformedEmployeeId = RandomString.make(10);
        additionalInformation.put(EMPLOYEE_ID, malformedEmployeeId);
        ValidationContext validationContext = ValidationContext.builder()
                .additionalInformation(additionalInformation)
                .build();
        ValidationResult validationResult = employeeExistenceValidationHandler.handle(validationContext);
        Assert.assertEquals(ValidationStatus.FAILED, validationResult.getValidationStatus());
        Assert.assertEquals(Validator.EMPLOYEE_EXISTENCE_VALIDATOR, validationResult.getValidator());
        Assert.assertEquals(String.format(ValidationErrorConstants.EMPLOYEE_NOT_FOUND, malformedEmployeeId), validationResult.getFailureReason());
    }

    @Test
    public void validationShouldFailIfTheEmployeeIdIsNotValid() {
        when(employeeRepository.getEmployeeById(ArgumentMatchers.anyInt()))
                .thenReturn(null);
        String validEmployeeId = "100000";
        Map<String, Object> additionalInformation = new HashMap<>();
        additionalInformation.put(EMPLOYEE_ID, validEmployeeId);
        ValidationContext validationContext = ValidationContext.builder()
                .additionalInformation(additionalInformation)
                .build();
        ValidationResult validationResult = employeeExistenceValidationHandler.handle(validationContext);
        Assert.assertEquals(ValidationStatus.FAILED, validationResult.getValidationStatus());
        Assert.assertEquals(Validator.EMPLOYEE_EXISTENCE_VALIDATOR, validationResult.getValidator());
        Assert.assertEquals(String.format(ValidationErrorConstants.EMPLOYEE_NOT_FOUND, validEmployeeId), validationResult.getFailureReason());
    }

    @Test
    public void validationShouldSucceedIfTheEmployeeIdIsValid() {
        when(employeeRepository.getEmployeeById(ArgumentMatchers.anyInt()))
                .thenReturn(new EmployeeEO());
        String validEmployeeId = "100000";
        Map<String, Object> additionalInformation = new HashMap<>();
        additionalInformation.put(EMPLOYEE_ID, validEmployeeId);
        ValidationContext validationContext = ValidationContext.builder()
                .additionalInformation(additionalInformation)
                .build();
        ValidationResult validationResult = employeeExistenceValidationHandler.handle(validationContext);
        Assert.assertEquals(ValidationStatus.SUCCESS, validationResult.getValidationStatus());
    }
}
