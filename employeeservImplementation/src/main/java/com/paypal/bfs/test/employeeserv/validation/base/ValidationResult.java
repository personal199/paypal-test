package com.paypal.bfs.test.employeeserv.validation.base;

import lombok.*;

import java.util.Collections;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class ValidationResult {

    private Validator validator;

    private ValidationStatus validationStatus;

    private String failureReason;

    private Map<String, Object> additionalInformation;

    public static ValidationResult success() {
        return ValidationResult.builder()
                .validationStatus(ValidationStatus.SUCCESS)
                .build();
    }

    public static ValidationResult failed() {
        return ValidationResult.builder()
                .validationStatus(ValidationStatus.FAILED)
                .additionalInformation(Collections.emptyMap())
                .build();
    }

    public ValidationResult at(Validator validator) {
        this.validator = validator;
        return this;
    }

    public ValidationResult dueTo(String failureReason) {
        this.failureReason = failureReason;
        return this;
    }

    public ValidationResult withAddedInformation(Map<String, Object> addedInformation) {
        this.additionalInformation = addedInformation;
        return this;
    }
}
