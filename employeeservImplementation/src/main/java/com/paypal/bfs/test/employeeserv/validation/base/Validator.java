package com.paypal.bfs.test.employeeserv.validation.base;

public enum Validator {
    EMPLOYEE_PAYLOAD_VALIDATOR,
    REQUEST_REPLAY_VALIDATOR,
    EMPLOYEE_EXISTENCE_VALIDATOR
}
