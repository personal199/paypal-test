package com.paypal.bfs.test.employeeserv.error;

import com.paypal.bfs.test.employeeserv.validation.base.ValidationResult;
import lombok.Getter;

@Getter
public class ValidationFailedException extends RuntimeException {

    private ValidationResult validationResult;

    public ValidationFailedException(ValidationResult validationResult) {
        this.validationResult = validationResult;
    }
}
