package com.paypal.bfs.test.employeeserv.validation.base;

public enum ValidationStatus {
    FAILED, SUCCESS
}
