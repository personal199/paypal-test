package com.paypal.bfs.test.employeeserv.validation.base;

import com.paypal.bfs.test.employeeserv.entity.Entity;
import com.paypal.bfs.test.employeeserv.entity.Operation;
import lombok.*;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ValidationContext {

    private String requestId;

    private Entity entityType;

    private Operation operation;

    private Object entity;

    private Map<String, Object> additionalInformation = new HashMap<>();
}
