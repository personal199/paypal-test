package com.paypal.bfs.test.employeeserv.validation.base;

public class ValidationErrorConstants {
    public static final String PAYLOAD_VALIDATION_FAILED = "PAYLOAD_VALIDATION_FAILED";
    public static final String REQUEST_REPLAYED = "Request replay detected. It could be due to client retries. The resource already created.";
    public static final String EMPLOYEE_NOT_FOUND = "Employee with id %s does not exist";
}
