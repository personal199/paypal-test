package com.paypal.bfs.test.employeeserv.error;

public class ErrorMessageConstants {

    public static final String ENTITY_NOT_FOUND = "The requested entity with id %s not found.";
}
