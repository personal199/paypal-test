package com.paypal.bfs.test.employeeserv.validation.impl;

import com.paypal.bfs.test.employeeserv.api.model.Employee;
import com.paypal.bfs.test.employeeserv.validation.base.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Scope(scopeName = "prototype")
@Component
@Slf4j
public class EmployeePayloadValidationHandler implements ValidationHandler {
    
    private ValidationHandler nextHandlerToInvoke;
    
    @Override
    public ValidationResult handle(ValidationContext validationContext) {
        Employee employee = (Employee) validationContext.getEntity();
        javax.validation.Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Employee>> constraintViolations = validator.validate(employee);
        if (!constraintViolations.isEmpty()) {
            log.error("Invalid employee payload. Request id : {}", validationContext.getRequestId());
            Map<String, Object> additionalInformation = constraintViolations.stream().collect(Collectors.toMap(cv -> cv.getPropertyPath().toString(), ConstraintViolation::getMessage));
            return ValidationResult.failed()
                    .at(getValidator())
                    .dueTo(ValidationErrorConstants.PAYLOAD_VALIDATION_FAILED)
                    .withAddedInformation(additionalInformation);
        }

        if (nextHandlerToInvoke != null) {
            return nextHandlerToInvoke.handle(validationContext);
        }
        return ValidationResult.success();

    }

    @Override
    public void setNext(ValidationHandler validationHandler) {
        nextHandlerToInvoke = validationHandler;
    }

    @Override
    public Validator getValidator() {
        return Validator.EMPLOYEE_PAYLOAD_VALIDATOR;
    }
}
