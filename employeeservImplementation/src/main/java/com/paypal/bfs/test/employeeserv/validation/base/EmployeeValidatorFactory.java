package com.paypal.bfs.test.employeeserv.validation.base;

import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class EmployeeValidatorFactory {

    private ListableBeanFactory beanFactory;

    @Autowired
    public void setBeanFactory(ListableBeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    private Map<Validator, ValidationHandler> getValidatorMap() {
        Collection<ValidationHandler> validationHandlers = beanFactory.getBeansOfType(ValidationHandler.class)
                .values();
        return validationHandlers.stream().collect(Collectors.toMap(ValidationHandler::getValidator, Function.identity()));
    }

    public ValidationHandler getEmployeeCreationValidator() {
        Map<Validator, ValidationHandler> validator = getValidatorMap();
        ValidationHandler payloadValidationHandler = validator.get(Validator.EMPLOYEE_PAYLOAD_VALIDATOR);
        ValidationHandler replayValidationHandler = validator.get(Validator.REQUEST_REPLAY_VALIDATOR);
        payloadValidationHandler.setNext(replayValidationHandler);
        replayValidationHandler.setNext(null);
        return payloadValidationHandler;
    }

    public ValidationHandler getEmployeeReadValidator() {
        Map<Validator, ValidationHandler> validator = getValidatorMap();
        ValidationHandler employeeExistenceValidator = validator.get(Validator.EMPLOYEE_EXISTENCE_VALIDATOR);
        employeeExistenceValidator.setNext(null);
        return employeeExistenceValidator;
    }

}
